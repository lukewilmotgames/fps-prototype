﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float blastConeAngle = 45;
    public float fireRate = 15f;

    public float recoilAmount = 0.1f;
    public float maxRecoil = -0.1f;
    private float currentRecoilPosZ;
    private float originalRecoilPosZ;

    private float nextTimeToFire = 0f;

    public ParticleSystem muzzleFlash;
    public ParticleSystem blastParticle;

    public GameObject endTransform;

    public LayerMask targetMask;

    public bool singleRay = false;

    [System.Serializable]
    public struct WeaponBase
    {
       public float damage;
       public float range;
       public float blastConeAngle; //Only for shotgun
       public float fireRate;
       public float recoilAmount;
       public float maxRecoil;
    }

    public WeaponBase shotgun = new WeaponBase();

    void Start()
    {
        originalRecoilPosZ = transform.position.z;
        currentRecoilPosZ = originalRecoilPosZ;
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();

            blastParticle.Play();

            if (currentRecoilPosZ <= 0)
            {
                Recoil();
            }
        }
    }

    void Shoot()
    {
        //if there is a muzzle flash then play muzzle flash
        if (muzzleFlash != null)
        {
            muzzleFlash.Play();
        }

        //Adding the recoil value to current Z position of weapon
        currentRecoilPosZ -= recoilAmount;

        CheckForTargets(endTransform.transform.position, range, damage);
    }

    //This checks for enemies within a radius from the gun
    void CheckForTargets(Vector3 location, float radius, float gunDamage) //Shotgun
    {
        //Get all colliders with the sphere radius
        Collider[] objectsInRange = Physics.OverlapSphere(location, radius);

        //Checking each collider found
        foreach (Collider col in objectsInRange)
        {
            //Finding the targets
            Target target = col.GetComponent<Target>();

            //If it found a target
            if (target != null)
            {
                //Damage Decay based on distances
                float proximity = (location - target.transform.position).magnitude;
                float effect = 1 - (proximity / radius);

                //Getting the angle between me and the target
                Vector3 dirToTarget = (target.transform.position - transform.position).normalized;

                //if target is within the blast cone angle 
                if (Vector3.Angle(transform.forward, dirToTarget) < blastConeAngle)
                {
                    //Distance between me and target
                    float distToTarget = Vector3.Distance(transform.position, target.transform.position);

                    //If the raycast hits the enemy 
                    if (Physics.Raycast(transform.position, dirToTarget, distToTarget, targetMask))
                    {
                        //Deal damage to target
                        target.TakeDamage(gunDamage * effect);                      
                    }
                }
            }
        }
    }

    void AssultRifle(float damage)
    {
        //Singular raycast to mouse, see what it hits
        RaycastHit hit;
        if (Physics.Raycast(endTransform.transform.position, endTransform.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();

            if (target != null)
            {
                target.TakeDamage(damage);
            }
        }
    }


    void Recoil()
    {
        //Clamping the recoil amount
        currentRecoilPosZ = Mathf.Clamp(currentRecoilPosZ, -maxRecoil, maxRecoil);

        //Returns current Z position back to original position
        currentRecoilPosZ = Mathf.SmoothDamp(currentRecoilPosZ, 0, ref originalRecoilPosZ, nextTimeToFire);

        //Applying recoil
        transform.localPosition = transform.localPosition + new Vector3(0, 0, currentRecoilPosZ);

        //Clamping the Z position so the recoil doesnt add on
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, Mathf.Clamp(transform.localPosition.z, -maxRecoil, maxRecoil));
    }

    //Visual of the cone
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        var rot = Quaternion.Euler(0, blastConeAngle, 0) * transform.forward;
        Gizmos.DrawRay(endTransform.transform.position, rot * range);
        var negRot = Quaternion.Euler(0, -blastConeAngle, 0) * transform.forward;
        Gizmos.DrawRay(endTransform.transform.position, negRot * range);
        var rot2 = Quaternion.Euler(0, 0, blastConeAngle) * transform.forward;
        Gizmos.DrawRay(endTransform.transform.position, rot * range);
    }
}
