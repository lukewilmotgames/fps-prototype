﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunSway : MonoBehaviour {

    /// <summary>
    /// Sway the gun slightly to the opposite side of the moving camera
    /// </summary>
    /// 

    public float amount;
    public float maxAmount;
    public float smoothAmount;

    private Vector3 initalPosition;

	void Start ()
    {
        initalPosition = transform.localPosition;
    }
	
	void Update ()
    {
        //X and Y values for how much sway
        float moveX = -Input.GetAxis("Mouse X") * amount;
        float moveY = -Input.GetAxis("Mouse Y") * amount;

        //Clamp how much the sway can move
        moveX = Mathf.Clamp(moveX, -maxAmount, maxAmount);
        moveY = Mathf.Clamp(moveY, -maxAmount, maxAmount);

        //Getting final Vector3 of position values
        Vector3 finalPos = new Vector3(moveX, moveY, 0);

        //Lerping the gun to its new position before returning
        transform.localPosition = Vector3.Lerp(transform.localPosition, finalPos + initalPosition, Time.deltaTime * smoothAmount);
    }
}
