﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpController : MonoBehaviour {

    /// <summary>
    /// Checks when the player wants to jump, 
    /// Check if the player is grounded, 
    /// Jump
    /// </summary>
    /// 

    public bool grounded = true;
    public float jumpForce = 10f;
    private bool jumpRequest = false;
    public float groundedSkin;
    public LayerMask groundLayer;

    Rigidbody rb;

    public GameObject sphereCheckLocation;
    private float sphereCheckRadius;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        sphereCheckRadius = GetComponent<CapsuleCollider>().radius;
    }

    private void Update()
    {
        grounded = CheckGrounded();

        if (grounded && Input.GetButtonDown("Jump"))
        {
            jumpRequest = true;
        }
    }

    void FixedUpdate ()
    {
        if (jumpRequest)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            jumpRequest = false;
        }
    }

    private bool CheckGrounded()
    {
        Collider[] objectsInRange = Physics.OverlapSphere(new Vector3(sphereCheckLocation.transform.position.x, sphereCheckLocation.transform.position.y - groundedSkin, sphereCheckLocation.transform.position.z), sphereCheckRadius, groundLayer);
        //foreach (Collider col in objectsInRange)
        //{
        //    Debug.Log(col.name);
        //}
        if (objectsInRange.Length > 0)
        {
            return true;
        }
        return false;
    }
}
