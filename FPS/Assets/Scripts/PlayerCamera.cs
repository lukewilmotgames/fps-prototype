﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

    /// <summary>
    /// Rotate the camera to the mouse position
    /// Rotate the player with the camera
    /// </summary>
    /// 

    private Vector2 mouseLook;
    private Vector2 smoothV;

    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;

    public float minClamp;
    public float maxClamp;

    private GameObject character;

    void Start ()
    {
        character = this.transform.parent.gameObject;
    }

	void Update ()
    {
        RotateCamera();
        RotateCharacter();
    }

    private void RotateCamera()
    {
        //Get the mouse inputs
        var md = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        //Mulitplies the mouse inputs by the sensitivity and smoothing values
        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));

        //Lerping the rotation between current point and the mouseInput with smoothing value to slow it down
        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
        mouseLook += smoothV;

        //Clamps the mouse between -90 and 90 so it stops when looking straight up or down
        mouseLook.y = Mathf.Clamp(mouseLook.y, minClamp, maxClamp);

        //Applying the rotation to camera
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
    }

    private void RotateCharacter()
    {
        //rotate the player with the x mouseInput
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
    }
}
