﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    /// <summary>
    /// Movement Script,
    /// 1. Vertical and horizontal movement
    /// 2. Smooth camera follow
    /// 3. Slight acceleration and deceleration when starting and stopping movement
    /// </summary> 
    /// 
    public float playerSpeed;
    Rigidbody rb;

    void Start ()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rb = GetComponent<Rigidbody>();
    }
	
	void Update ()
    {
        transform.Translate(MovementInputs());

        Escape();
    }

    //Creates a vector3 with the horizontal and vertical inputs and returns it
    private Vector3 MovementInputs()
    {
        Vector3 movementInputs = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * Time.deltaTime * playerSpeed;

        return movementInputs;
    }

    //Unlocks mouse 
    void Escape()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
