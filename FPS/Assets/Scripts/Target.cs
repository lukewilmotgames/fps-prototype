﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    /// <summary>
    /// Used as a base health script for the enemies
    /// Controls how much health an enemy has
    /// Controls when the enemy dies
    /// </summary>
    /// 

    public float health = 50f;

    public void TakeDamage(float damageAmount)
    {
        health -= damageAmount;
        if (health <= 0f)
        {
            Die();
        }
    }

    void Die()
    {
        Destroy(this.gameObject);
    }

}
