﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{

    /// <summary>
    /// Holds the code for all basic weapons
    /// </summary>

    private float nextTimeToFire = 0f;
    private float currentRecoilPosZ;
    private float originalRecoilPosZ;

    //Weapon types
    public enum Weapons
    {
        shotGun,
        machineGun,
    }
    public Weapons weapons;

    //Weapon Data
    [System.Serializable]
    public struct WeaponBase
    {
        public float damage;
        public float range;
        public float blastConeAngle; //Only for shotgun
        public float fireRate;
        public float recoilAmount;
        public float maxRecoil;
        public ParticleSystem muzzleFlash;
        public ParticleSystem hitParticle;
        public GameObject endTransform;
        public LayerMask targetMask;
    }

    //Base Weapons
    public WeaponBase shotGun = new WeaponBase();
    public WeaponBase machineGun = new WeaponBase();
    private WeaponBase currentWeapon;


    void Start()
    {
        originalRecoilPosZ = transform.position.z;
        currentRecoilPosZ = originalRecoilPosZ;
    }

    void Update()
    {
        SelectWeapon();

        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / currentWeapon.fireRate;
            Shoot();

            if (currentRecoilPosZ <= 0)
            {
                Recoil();
            }
        }
    }

    //Change weapon based on weapon state
    void SelectWeapon()
    {
        switch (weapons)
        {
            case Weapons.shotGun:
                currentWeapon = shotGun;
                break;

            case Weapons.machineGun:
                currentWeapon = machineGun;
                break;
        }
    }

    void Shoot()
    {
        //if there is a muzzle flash then play muzzle flash
        if (currentWeapon.muzzleFlash != null)
        {
            currentWeapon.muzzleFlash.Play();
        }

        //Adding the recoil value to current Z position of weapon
        currentRecoilPosZ -= currentWeapon.recoilAmount;

        if (weapons == Weapons.shotGun)
        {
            CheckForTargets(currentWeapon.endTransform.transform.position, currentWeapon.range, currentWeapon.damage);
        }
        else
        {
            AssultRifle(currentWeapon.damage);
        }
    }

    //This checks for enemies within a radius from the gun
    void CheckForTargets(Vector3 location, float radius, float gunDamage) //Shotgun
    {
        //Get all colliders with the sphere radius
        Collider[] objectsInRange = Physics.OverlapSphere(location, radius);

        //Checking each collider found
        foreach (Collider col in objectsInRange)
        {
            //Finding the targets
            Target target = col.GetComponent<Target>();

            //If it found a target
            if (target != null)
            {
                //Damage Decay based on distances
                float proximity = (location - target.transform.position).magnitude;
                float effect = 1 - (proximity / radius);

                //Getting the angle between me and the target
                Vector3 dirToTarget = (target.transform.position - transform.position).normalized;

                //if target is within the blast cone angle 
                if (Vector3.Angle(transform.forward, dirToTarget) < currentWeapon.blastConeAngle)
                {
                    //Distance between me and target
                    float distToTarget = Vector3.Distance(transform.position, target.transform.position);

                    //If the raycast hits the enemy 
                    if (Physics.Raycast(transform.position, dirToTarget, distToTarget, currentWeapon.targetMask))
                    {
                        //Deal damage to target
                        target.TakeDamage(gunDamage * effect);

                    }
                }
            }
        }
    }

    void AssultRifle(float damage)
    {
        //Singular raycast to mouse, see what it hits
        RaycastHit hit;
        if (Physics.Raycast(currentWeapon.endTransform.transform.position, currentWeapon.endTransform.transform.forward, out hit, currentWeapon.range))
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();

            if (target != null)
            {
                //Damage Decay based on distances
                float proximity = (currentWeapon.endTransform.transform.position - target.transform.position).magnitude;
                float effect = 1 - (proximity / currentWeapon.range);
                target.TakeDamage(damage * effect);
            }
        }
    }

    void Recoil()
    {
        //Clamping the recoil amount
        currentRecoilPosZ = Mathf.Clamp(currentRecoilPosZ, -currentWeapon.maxRecoil, currentWeapon.maxRecoil);

        //Returns current Z position back to original position
        currentRecoilPosZ = Mathf.SmoothDamp(currentRecoilPosZ, 0, ref originalRecoilPosZ, nextTimeToFire);

        //Applying recoil
        transform.localPosition = transform.localPosition + new Vector3(0, 0, currentRecoilPosZ);

        //Clamping the Z position so the recoil doesnt add on
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, Mathf.Clamp(transform.localPosition.z, -currentWeapon.maxRecoil, currentWeapon.maxRecoil));
    }
}
